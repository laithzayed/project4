<?php
include "includes/connection.php";

if (isset($_POST['submit'])) {
    //Defining variables
    $email    = $_POST['email'];
    $password = $_POST['password'];
    $errors   = array();

    //Creating query to check admins table for normal user
    $check_normal_admin_query = "SELECT * FROM admins WHERE admin_email    = '{$_POST['email']}'
                                                            AND admin_password = '{$_POST['password']}'
                                                            AND admin_role     = 'admin' ";
    $normal_admin_result = mysqli_query($conn, $check_normal_admin_query);
    $normal_admin_rows = mysqli_fetch_assoc($normal_admin_result);
    //Creating query to check admins table for super user
    $check_super_admin_query = "SELECT * FROM admins WHERE  admin_email    = '{$_POST['email']} '
                                                            AND admin_password = '{$_POST['password']}'
                                                            AND admin_role     = 'superAdmin' ";
    $super_admin_result = mysqli_query($conn, $check_super_admin_query);
    $super_admin_rows = mysqli_fetch_assoc($super_admin_result);

    //Checking that no inputs are empty
    if (empty($_POST['email'])) {
        $error1 = "Please enter email <br>";
        array_push($errors, $error1);
        echo $error1;
    }
    if (empty($_POST['password'])) {
        $error2 = "Please enter password <br>";
        array_push($errors, $error2);
        echo $error2;
    }

    if (!empty($_POST['email']) && !empty($_POST['password'])) {

        //Checking normal admin
        if ($normal_admin_rows) {
            echo "hello normal admin";
        }

        //Checking super admin
        elseif ($super_admin_rows) {
            echo "hello super admin";
        } else {
            echo "incorrect email or password";
        }
    }
}
?>

<!DOCTYPE html>
<html class="no-js" lang="en">

<body>
    <!--Form Sign In-->
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="signin-container">
            <form name="frm-login" method="post">
                <p class="form-row">
                    <label for="fid-name">Email Address:<span class="requite">*</span></label>
                    <input type="text" id="fid-name" name="email" value="" class="txt-input">
                </p>
                <p class="form-row">
                    <label for="fid-pass">Password:<span class="requite">*</span></label>
                    <input type="password" id="fid-pass" name="password" value="" class="txt-input">
                </p>
                <p class="form-row wrap-btn">
                    <button class="btn btn-submit btn-bold" type="submit" name="submit">sign in</button>
                    <!-- <a href="#" class="link-to-help">Forgot your password</a> -->
                </p>
            </form>
        </div>
    </div>
</body>

</html>